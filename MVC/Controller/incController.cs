﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC.Interface;

namespace MVC.Controller
{
    // کلاس این کنترلر اینترفیس IControler را پیاده سازی کرده
    // که Model و View را به یکدیگر متصل میسازد.
    // همچنین این کلاس از طریق ImodelInterface به view متصل شده و اداره کننده رخداد را به متد view_changed اضافه مینماید.
    public class incController: IController
    {
        IView view;
        IModel model;

        public incController(IView v,IModel m)
        {
            view = v;
            model = m;
            view.setController(this);
            model.attach((ImodelObserver) view);
            view.changed+=new ViewHandler<IView>(this.view_changed);
        }

        //زمانی که کاربر از طریق View در واقع همان دکمه ای که در Form وجود دارد مقدار را تغییر دهد
        // این رخداد Fier میشود و در نتیجه این متد اجرا میشود 
        public void view_changed(IView v, ViewEventArgs e)
        {
            model.setvalue(e.value);
        }

        // عمل افزایش مقدار در واقع توسط Model انجام شده و کنترلر صرفا در نقش واسطه عمل میکند
        public void incvalue()
        {
            model.increment();
        }
    }
}
