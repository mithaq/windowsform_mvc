﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC.Interface
{
    // این اینترفیس تنها یک کار را به عهده دارد و آن یک واحد افزایش value میباشد. 
    public interface IController
    {
        void incvalue();
    }
}
