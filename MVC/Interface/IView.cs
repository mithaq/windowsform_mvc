﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVC.Interface
{
    public delegate void ViewHandler<IView>(IView sender, ViewEventArgs e);
    
    // کلاس ViewEventArgs از کلاس EventArgs ارث بری کرده 
    //و زمانی از آن استفاده میشود که کاربر مقدار را افزایش داده و رخداد آن اتفاق بیفتد  
    public class ViewEventArgs : EventArgs
    {
        public int value;

        public ViewEventArgs(int v){value = v;}
    }

    // اینترفیس IView دارای متدی است که وظیفه آن متصل کر دن View به Controler است.
    // عضو دیگر اینترفیس IView رخداد changed بوده که این رخداد در متد سازنده کلاس incController مقداردهی میشود
    public interface IView
    {
        event ViewHandler<IView> changed;
        void setController(IController cont);
    }
}
